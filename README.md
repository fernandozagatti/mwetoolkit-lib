mwetoolkit-lib
===========
Adaptation of the *Multiword Expressions toolkit* (mwetoolkit3) as a python library



* Official website: http://mwetoolkit.sf.net
* Official project repo: https://gitlab.com/mwetoolkit/mwetoolkit3/
* Latest release: [version 3.0](https://gitlab.com/mwetoolkit/mwetoolkit3/repository/archive.zip?ref=v3.0)
* Release date: January 11, 2018
* Authors: Carlos Ramisch, Silvio Ricardo Cordeiro, Vitor de Araujo, Sandra Castellanos
* Email: `"{usr}@{host}".format(usr="mwetoolkit",host=".".join(["gmail","com"]))`

The mwetoolkit aids in the automatic identification and extraction of [multiword 
expressions (MWEs)](https://en.wikipedia.org/wiki/Multiword_expression) from 
running text. These include idioms (*kick the bucket*), noun compounds (*cable 
car*), phrasal verbs (*take off, give up*), etc. 

Even though it focuses on multiword expresisons, the framework is quite complete 
and can be useful in any corpus-based study in computational linguistics. The 
mwetoolkit can be applied to virtually any text collection, language, and MWE 
type. It is a command-line tool written mostly in Python3. Its development 
started in 2010 as a PhD thesis but the project keeps active (see commit logs).

Up-to-date documentation and details about the tool can be found at the 
mwetoolkit website: http://mwetoolkit.sourceforge.net/

#### About the mwetoolkit-lib

It's a project to turn mwetoolkit3 into a python library that can be easily integrated into pipelines.

* Authors: Fernando Rezende Zagatti, Paulo Augusto de Lima Medeiros, Esther da Cunha Soares, Lucas Nildaimon dos Santos Silva, Carlos Ramisch, Livy Real

The main method of the **mwetoolkit-lib**, to be called by the user in a python script, was built inside the **mwetoolkitlib.py** file and must be accessed by calling the **get_candidates_dataframe** method.
The idea here is to encapsulate all intermediate steps into a single function, hiding unimportant details about the tool's internal architecture from the users, leaving the pipeline less prone to human errors.
    
It is necessary to pass two parameters to the method, namely: 
(1) a corpus file containing the corpus from which the MWEs will be discovered with the POS tags, lemmas and surface forms for each token and 
(2) a file containing the morphosyntactic patterns that the user wants to extract from the text.
Both files can be presented in any format supported by the **mwetoolkit**.
This method will return a **pandas** dataframe with MWE candidates and their info.
As in the original **mwetoolkit**, candidates are shown in their normalized form (lemmas) alongside with their POS tags, occurrences count and AMs.
Ranking and filtering can now be easily done using **pandas** and data can be integrated with other python libraries.

**An example usage is found in /mwetoolkit-lib test/mwetookit-lib test.ipynb**

### 1) INSTALLING
    
Please refer to the [website](http://mwetoolkit.sf.net) for up-to-date 
installation instructions.

This version of the toolkit requires python3. For a legacy version supporting
python2, you can clone the [legacy repository](https://gitlab.com/mwetoolkit/mwetoolkit2-legacy/).

If you want to install the official mwetoolkit, you can clone the [repository](https://gitlab.com/mwetoolkit/mwetoolkit3).

### 2) QUICK START
    
To install the mwetoolkit-lib, just download it from the GIT repository using the 
following command:

    git clone --depth=1 "https://gitlab.com/fernandozagatti/mwetoolkit-lib.git"

As the code evolves fast, we recommend you to use the GIT version instead of old
releases. Periodically `git pull` to have access to latest improvements.

Once you have downloaded the toolkit, navigate to the main folder and run the 
command below for compiling the C libraries used by the toolkit.[^1]

    make

### 3) DOCUMENTATION

The [website](http://mwetoolkit.sf.net) contains tutorials and examples 
to get started, as well as detailed descriptions of the tools and file
formats manipulated by the toolkit.

### 4) REGRESSION TESTS
    
The `test` folder contains regression tests for most scripts. In order to test
your installation of the mwetoolkit, navigate to this folder and then call the
script `testAll.sh`

    cd test
    ./testAll.sh

Should one of the tests fail[^2], please send a copy of the output and a brief
description of your configurations (operating system, version, machine) to our
email.



[^1]: If you do not run this command, the toolkit will still work but it will use a Python version (much slower and possibly obsolete!) of the indexing and counting scripts. This may be OK for small corpora.
[^2]: Please, beware that on Mac OS some test will appear to fail when they actually succeed, the only differences being in rounding less significant digits of float numbers.
