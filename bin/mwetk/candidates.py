from mwetk.base.candidate import CandidateFactory
from mwetk.base.frequency import Frequency
from mwetk.base.ngram import Ngram
from mwetk.filetype import parse_gen, parse_entities
from mwetk.base.sentence import Sentence

import collections

def iter_sentences(file_list):
    for entity in parse_gen(file_list):
        if type(entity) is Sentence:
            yield entity


def get_patterns(file_list):
    return parse_entities(file_list)


def extract_candidates_from_sentences(sentences, dataset_name, patterns):
    all_entities = collections.OrderedDict()
    for sentence in sentences:
        add_matches_from_sentence(all_entities, patterns, sentence, dataset_name)
    return build_cands(all_entities)


def extract_candidates_from_file(dataset_name, file_path, patterns):
    return extract_candidates_from_sentences(iter_sentences([file_path]), dataset_name, patterns)


def add_matches_from_sentence(all_entities, input_patterns, sentence, current_corpus_name, match_distance="All", id_order=["*"], non_overlapping=False, ignore_pos=False, surface_instead_lemmas=False):
    already_matched = set()

    # All entities will be updated with new matches

    for pattern in input_patterns:
        for (match_ngram, wordnums) in pattern.matches(sentence,
                match_distance=match_distance, id_order=id_order,
                overlapping=not non_overlapping):
            wordnums_string = ",".join(str(wn+1) for wn in wordnums)
            if wordnums_string in already_matched:
                continue
            already_matched.add( wordnums_string )

            if ignore_pos :    
                match_ngram.foreach_del_prop("pos")
            ngram_basestring_specific = str(match_ngram.to_string())

            if surface_instead_lemmas:
                match_ngram.foreach_del_prop("lemma")
            else :
                for word in match_ngram:
                    # (Still uses surface if lemma is unavailable)
                    if word.has_prop("lemma"):
                        word.del_prop("surface")

            # TODO create a CandidateSet class to abstract away this grouping
            # of stuff based on general-vs-specific info
            ngram_basestring_general = str(match_ngram.to_string())

            # Returns reference (python) for value with key=ngram_basestring_general
            # If there is no object with this key, a new one is created 
            info_for_ngram_basestring = all_entities.setdefault(
                    ngram_basestring_general, {})
            (surfaces_dict, total_freq) = info_for_ngram_basestring \
                    .get(current_corpus_name, ({}, 0))
            freq_surface = surfaces_dict.setdefault(ngram_basestring_specific, [])

            # Append the id of the source sentence. The number of items in
            # surfaces_dict[form] is the number of occurrences of that form.
            source_sent_id = str(sentence.id_number) + ":" + wordnums_string
            surfaces_dict[ ngram_basestring_specific ].append( source_sent_id )
            info_for_ngram_basestring[current_corpus_name] \
                    = (surfaces_dict, total_freq + 1)


def build_cands(allEntities, print_cand_freq = False, print_source = False, ctxinfo=None):
    candFact = CandidateFactory()
    
    cands = []
    for ngram_basestring_general, info in allEntities.items() :
        cand = candFact.make()
        cand.from_string(ctxinfo, ngram_basestring_general)
        for corpus_name, (surface_dict, total_freq) in info.items():
            if print_cand_freq :
                freq = Frequency( corpus_name, total_freq )
                cand.add_frequency( freq )
            for occur_string in list(surface_dict.keys()) :
                occur_form = Ngram( None, None )
                occur_form.from_string(ctxinfo, occur_string)
                sources = surface_dict[occur_string]
                freq_value = len(sources)
                freq = Frequency( corpus_name, freq_value )
                occur_form.add_frequency( freq )
                if print_source:
                    occur_form.add_sources(sources)
                cand.add_occur( occur_form )
        
        cands.append(cand)

    return cands