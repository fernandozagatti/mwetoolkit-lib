import math
from shlex import join
from typing import NoReturn 

from mwetk.base.feature import Feature


def append_metrics_to_candidates(candidates, corpussize_dict, measures, main_freq=None, heuristic_combine=None, not_normalize_mle=False, warn_ll_bigram_only=False, ctxinfo=None):
    if heuristic_combine is None:
        heuristic_combine = lambda l : sum( l ) / len( l ) # Arithmetic mean

    for candidate in candidates:
        compute_metrics(ctxinfo, candidate, corpussize_dict, main_freq, measures, heuristic_combine, not_normalize_mle, warn_ll_bigram_only)


def compute_metrics(ctxinfo, candidate, corpussize_dict, main_freq, measures, heuristic_combine, not_normalize_mle, warn_ll_bigram_only):
    """
        This function will compute the association metrics for the candidate in each corpus it occours
    """
    # Could this be a method of the Candidate class?

    joint_freq = {}
    singleword_freq = {}
    backed_off = False

    for freq in candidate.freqs : # There is a freq for each dataset
        joint_freq[ freq.name ] = ( float(abs( freq.value ) ) ) # Convert all these integers to floats...
        singleword_freq[ freq.name ] = [] # Initializing singleword_freq dict
        if freq.value < 0 :
            backed_off = True
    

    for word in candidate :
        for freq in word.freqs :
            singleword_freq[ freq.name ].append( abs( float(freq.value) ) )
            # Little trick: negative counts indicate backed-off counts
            if freq.value < 0 :
                backed_off = True
    
    for freq in candidate.freqs :
        corpus_name = freq.name
        if not backed_off and corpus_name == "backoff" :
            N = corpussize_dict[ main_freq ]
        else :
            N = corpussize_dict[ corpus_name ]
        
        # Compute different feats and add them to candidates
        feats = calculate_ams( ctxinfo, joint_freq[ corpus_name ],
                singleword_freq[ corpus_name ],
                N, corpus_name, measures, heuristic_combine, not_normalize_mle, warn_ll_bigram_only )

        # Feature name will be: {corpus_name}_{metric}. So metrics from different datasets won't mix up
        for feat in feats :
            candidate.add_feat( feat )

    


################################################################################

def calculate_ams( ctxinfo, o, m_list, N, corpus_name, measures, heuristic_combine, not_normalize_mle, warn_ll_bigram_only ) :
    """
        Given a joint frequency of the ngram, a list of individual frequencies,
        a corpus size and a corpus name, generates a list of `Features`, each
        containing the value of an Association Measure.
        
        @param o The float value corresponding to the number of occurrences of 
        the ngram.
        
        @param m_list A list of float values corresponding to the number of 
        occurrences of each of the words composing the ngram. The list should
        NEVER be empty, otherwise the result is undefined.
        
        @param N The float value corresponding to the number of tokens in the
        corpus, i.e. its total size. The size of the corpus should NEVER be 
        zero, otherwise the result is undefined.
        
        @param corpus_name A string that uniquely identifies the corpus from
        which the counts were drawn.        
    """
    # N is never null!!!
    # m_list is never empty!!!
    feats = []
    f_sum = 0
    n = len( m_list )
    e = expect( m_list, N )
    if "mle" in measures :
        if not_normalize_mle :
            mle = int( o )
        else :
            mle = o / N
        feats.append( Feature( "mle_" + corpus_name, mle ) )
    if "pmi" in measures :
        if e != 0 and o != 0:
            pmi = math.log( o / e, 2 )
        else :
            pmi = 0.0
        feats.append( Feature( "pmi_" + corpus_name, pmi ) )
    if "t" in measures :
        if o != 0.0 :
            t = ( o - e ) / math.sqrt( o )
        else :
            t = 0.0
        feats.append( Feature( "t_" + corpus_name, t ) )
    if "dice" in measures :
        if sum( m_list ) != 0.0 :
            dice = ( n * o ) / sum( m_list )
        else :
            dice = 0.0
        feats.append( Feature( "dice_" + corpus_name, dice ) )
    if "ll" in measures :
        #pdb.set_trace()
        if len( m_list ) == 2 :
            # Contingency tables observed, expected
            ( ct_os, ct_es ) = contingency_tables( ctxinfo, [o], m_list, N, corpus_name )
            ll_list  = [] # Calculation is suitable for generic ngrams
            for (ct_o, ct_e) in zip( ct_os, ct_es ) :
                ll = 0.0
                for i in range( 2 ) :
                    for j in range( 2 ) :
                        if ct_o[i][j] != 0.0 :
                            
                            ll += ct_o[i][j] * ( math.log( ct_o[i][j], 10 ) -
                                                 math.log( ct_e[i][j], 10 ) )
                ll *= 2
                ll_list .append( ll )
            ll_final = heuristic_combine( ll_list )
        else :
            if warn_ll_bigram_only:
                warn_ll_bigram_only = False
                ctxinfo.warn("log-likelihood is only implemented for 2grams. "
                     "Defaults to 0.0 for n>2")
            ll_final = 0.0
        feats.append( Feature( "ll_" + corpus_name, ll_final ) )
    return feats


################################################################################     

def contingency_tables( ctxinfo, bigram_freqs, unigram_freqs, N, corpus_name ):
    """
        Given an ngram (generic n) w_1 ... w_n, the input is a couple of lists
        containing integer frequencies, the output is a couple of lists with
        contingency tables. The first list contains bigram frequencies
        [ f(w_1 w_2), f(w_2 w_3), ..., f(w_n-1 w_n) ]. The second list contains
        unigram frequencies [ f(w_1), f(w_2), ..., f(w_n) ]. While the first
        list contains n-1 elements, the second list contains n elements. The
        result is a couple of lists with contingency tables, the first
        corresponds to the observed frequencies, the second to expected
        frequencies. The contingency tables are 2D lists that contain the 4
        possible outcomes for the occurrence of a bigram, i.e. c(w1 w2),
        c(w1 ~w2), c(~w1 w2) and c(~w1 ~w2), where "~w" means "any word but w".
        Observed contingency tables are exact calculations based on simple
        set operations (intersection, difference). The expected frequencies are
        calculated using maximum likelihood for independent events (e.g. the
        occurrence of w1 does not change the probability of the occurrence of w2
        or of ~w2 imediately after w1, also noted P(w2|w1)=P(w2)).

        @param bigram_freqs List of integers representing bigram frequencies.
        Notice that no bigram can occur more than the words that is contains.
        Any inconsistency will be automatically corrected and output as a
        warning. This list should contain n-1 elements.

        @param unigram_freqs List of integers representing unigram (word)
        frequencies. This list should have n elements.

        @param corpus_name The name of the corpus from which frequencies were
        drawn. This is only used in verbose mode to provide friendly output
        messages.

        @return a couple (observed, expected), where observed and expected are
        lists, both of size n-1, and each cell of each list contains a 2x2 table
        with observed and expected contingency tables for the bigrams given as
        input.
    """
    observed = []
    expected = []
    n = len( unigram_freqs )
    if len( bigram_freqs ) != n - 1 :
        ctxinfo.warn( "Invalid unigram/bigram frequencies passed to "
              "calculate_negations function")
        return None

    # 1) Verify that all the frequencies are valid
    for i in range( len( bigram_freqs ) ) :
        if bigram_freqs[ i ] > unigram_freqs[ i ] or \
           bigram_freqs[ i ] > unigram_freqs[ i + 1 ] :
            ctxinfo.warn( corpus_name + " unigrams must occur at least as much as bigram.")
        if bigram_freqs[ i ] > unigram_freqs[ i ] :
            ctxinfo.warn("Automatic correction: " + \
                                 str( unigram_freqs[ i ] ) + " -> " + \
                                 str( bigram_freqs[ i ] ))
            unigram_freqs[ i ] = bigram_freqs[ i ]
        if bigram_freqs[ i ] > unigram_freqs[ i + 1 ] :            
            ctxinfo.warn("Automatic correction: " + \
                                 str( unigram_freqs[ i + 1 ] ) + " -> " + \
                                 str( bigram_freqs[ i ] ))
            unigram_freqs[ i + 1 ] = bigram_freqs[ i ]

    # 2) Calculate negative freqs 
    for i in range( len( bigram_freqs ) ) :        
        o = [ 2 * [ -1 ], 2 * [ -1 ] ]
        e = [ 2 * [ -1 ], 2 * [ -1 ] ]
        cw1 = unigram_freqs[ i ]
        cw2 = unigram_freqs[ i + 1 ]
        cw1w2 = bigram_freqs[ i ]        
        o[ 0 ][ 0 ] = cw1w2
        e[ 0 ][ 0 ] = expect( [ cw1, cw2 ], N )
        o[ 0 ][ 1 ] = cw1 - cw1w2
        e[ 0 ][ 1 ] = expect( [ cw1, N - n + 1 - cw2 ], N )
        o[ 1 ][ 0 ] = cw2 - cw1w2
        e[ 1 ][ 0 ] = expect( [ N - n + 1 - cw1, cw2 ], N )
        # BEWARE! THERE WAS A HUGE ERROR HERE, CORRECTED ON APRIL 18, 2012
        # ALL LOG-LIKELIHOOD VALUES CALCULATED BY THE TOOLKIT WERE WRONG!
        # PLEASE RE-RUN IF YOU USED THE OLD VERSION!
        o[ 1 ][ 1 ] = N - len( unigram_freqs )  + 1 - cw1 - cw2 + cw1w2 
        e[ 1 ][ 1 ] = expect( [ N - n + 1 - cw1, N - n + 1 - cw2 ], N )
        observed.append( o )
        expected.append( e )
    return (observed, expected)

################################################################################

def expect( m_list, N ):
    """
        Returns the expected joint frequency of the n-gram by multiplying the
        individual frequencies of each word divided by N, and then scaling it
        back to N. That is, given c(w_1), c(w_2), ..., c(w_n) counts of words
        w_1 to w_n, and the size of the corpus N, we calculate the expected
        count E(w_1, ..., w_n) as:
        
                             w_1   w_2         w_n         w_1 x w_2 x ... x w_n
        E(w_1, ..., w_n) = ( --- x --- x ... x --- ) x N = ---------------------
                              N     N           N                 N^(n-1)
                              
        Actually, N is adjusted to correspond to the number of n-grams and not
        of tokens in the corpus, i.e. N - n + 1 instead of N. For large values 
        of N, this is neglectable.
        
        @param m_list List of the individual word counts. The list has n float
        elements, the same size of the n-gram.
        
        @param N Number of total tokens in the frequency source.
        
        @return A single float value that corresponds to E(w_1, ..., w_n) as
        calculated in the formula above.
    """
    result = 1.0
    for i in range(len(m_list)) :
        result *= m_list[ i ] / N
    return result * ( N - len(m_list) + 1 )