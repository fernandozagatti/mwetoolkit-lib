import pandas as pd
from importlib import reload
from tqdm.notebook import tqdm

import mwetk

from mwetk.candidates import *
from mwetk.index import *
from mwetk.filetype import indexlib
from mwetk.counter import append_counters_to_candidates
from mwetk.feat_association import append_metrics_to_candidates

def build_df_from_candidates(candidates, dataset_name, measures):  

    columns = ['ngram', 'pos', 'counter'] + measures
    infos = { col:[] for col in columns }
    for candidate in candidates:
        infos['ngram'].append(' '.join([x.lemma for x in candidate.word_list]))
        infos['pos'].append(' '.join([x.pos for x in candidate.word_list]))
        infos['counter'].append(candidate.freqs.get_feature(dataset_name, -1).value)
        
        for measure in measures:
            feature_name = f"{measure}_{dataset_name}"
            infos[measure].append(candidate.features.get_feature(feature_name, -1).value)
    
    return pd.DataFrame(infos) 

def get_candidates_dataframe(patterns_file_path, corpus_file_path, dataset_name='dataset', attr_combination="lemma+pos", measures=None):
    if measures is None:
        measures=['mle', 't', 'pmi', 'dice', 'll']
    
    patterns = get_patterns([patterns_file_path])
    
    cands = extract_candidates_from_file(dataset_name, corpus_file_path, patterns)
    
    index = build_index(
        dataset_name,
        [corpus_file_path], 
        simples_attributes=attr_combination.split('+'),
        composite_attributes=[attr_combination],
        ctxinfo=None
    )
    
    corpus_size_dict = {dataset_name: index.metadata["corpus_size"]}
    
    append_counters_to_candidates(index, attr_combination, cands, dataset_name)
    
    append_metrics_to_candidates(cands, corpus_size_dict, measures)
    
    df = build_df_from_candidates(cands, dataset_name, measures)
    
    return df

