from mwetk.filetype import indexlib

from mwetk.filetype.indexlib import Index, ATTRIBUTE_SEPARATOR

def build_entry_builder(composite_rule):
    # surface, lemma, pos
    attributes = composite_rule.split('+')


    def build_entry(surface, lemma, pos):
        return (
            (surface + ATTRIBUTE_SEPARATOR if "surface" in attributes else "") + 
            (lemma + ATTRIBUTE_SEPARATOR if "lemma" in attributes else "") + 
            (pos if "pos" in attributes else "")
        )
    return build_entry



def build_index(corpus_name, corpus_file_list, simples_attributes=["lemma", "pos"], composite_attributes=["lemma+pos"], ctxinfo=None):
    index = indexlib.Index(corpus_name, simples_attributes, ctxinfo)
    index.populate_index(corpus_file_list, None, ctxinfo)
    for attr in composite_attributes:
        index.make_fused_array(attr.split('+'), ctxinfo)
    return index


