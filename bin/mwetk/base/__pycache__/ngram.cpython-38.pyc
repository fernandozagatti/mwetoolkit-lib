U
    �x&aOE  �                   @   sX   d Z ddlZddlmZ ddlmZ ddlmZmZm	Z	 ddl
mZ G d	d
� d
e�ZdS )z�
    This module provides the `Ngram` class. This class represents an ngram, i.e.
    a sequence of words as they occur in the corpus. A ngram is any sequence of
    n words, not necessarily a linguistically motivated phrase.
�    N�   )�util�   )�
FeatureSet)�Word�	SEPARATOR�WORD_SEPARATOR)�	Frequencyc                   @   s�   e Zd ZdZd5dd�Zdd� Zdd� Zd	d
�dd�Zdd� Zdd� Z	dd� Z
dd� Zdd� Zdd� Zdd� Zdd� Zdd� Zdd � Zd!d"� Zd#d$� Zd%d&� Zed'�d(d)�Zd*d+� Zd,d-� Zd6d/d0�Zd7d1d2�Zd8d3d4�ZdS )9�Ngrama{  
        A `Ngram` is a sequence of n adjacent words. For example, an ngram with 
        2 adjacent words is called bigram and has n=2. An ngram with 3 words is 
        called trigram and has n=3. Besides the list of words composing the 
        ngram, the class also has a list of frequencies that correspond to the
        number of occurrences of the ngram in a corpus.
    Nc                 C   sL   |pg | _ |dks$t|t�s$t|��|p2tdtj�| _|rBt|�ng | _dS )aN  
            Instanciates the `Ngram` with the list of words that compose it and
            the list of frequencies associated to the ngram. 
            
            @param word_list A list of `Word`s corresponding to the sequence of
            adjacent tokens found in the corpus. The size of the list will
            determine the value of n, no verification is made in order to 
            constraint the size of the list, i.e. you can define an empty ngram,
            a 1-gram (ngram with a single word) or even 400-grams if you want.
            
            @param freqs A list of `Frequency`ies corresponding to counts of 
            occurrences of this ngram in a certain corpus. Please notice that
            the frequencies correspond to occurrences of ALL the words of the
            ngram in a corpus. Individual word frequencies are attached to the
            corresponding `Word` object in the `word_list`. 

            @param sources A list of the ids of the sentences where the
            ngram occurs.
            
            @return A new instance of a `Ngram`.
        N�freq)	�	word_list�
isinstancer   �AssertionError�operator�add�freqs�list�sources)�selfr   r   r   � r   �9/home/paulo/Repos/mwetoolkit3-dev/bin/mwetk/base/ngram.py�__init__5   s    
zNgram.__init__c                 C   sX   dd� | j D �}| jr"d�| j�nd}| jr8d�| j�nd}d�t| �jd�|�||�S )Nc                 S   s   g | ]}|j p|jpd �qS )z???)�surface�lemma��.0�wr   r   r   �
<listcomp>T   s     z"Ngram.__repr__.<locals>.<listcomp>z, freqs={!r}� z, sources={!r}z{}(<{}>{}{})�_)r   r   �formatr   �type�__name__�join)r   ZwordsZs_freqsZ	s_sourcesr   r   r   �__repr__S   s    zNgram.__repr__c                 C   s    | j �|j � | j�|j� dS )zMerge `other` into `self`.N)r   �
merge_fromr   �extend�r   �otherr   r   r   r%   \   s    zNgram.merge_fromr   )�wordc                 C   s   | j �|� dS )a�  
            Append a `Word` to the end of the list of words of the ngram.
            
            @param word `Word` that corresponds to an adjacent token of this 
            ngram in a corpus. Should contain at least one of the three 
            components of a word (surface form, lemma or POS tag), unless you 
            want to explicitely concatenate an empty word to the end of the 
            ngram. No test is performed in order to verify whether this is a 
            repeated word in the list or whether the ngram is linguistically 
            well-formed. This means that if you concatenate words that do not 
            make sense together (e.g. "the of going never"), it is basically 
            your problem.
        N)r   �append)r   r)   r   r   r   r*   d   s    zNgram.appendc                 C   s   | j �|j|j� dS )a,  
            Add a `Frequency` to the list of frequencies of the ngram.
            
            @param freq `Frequency` that corresponds to a count of this ngram in 
            a corpus. No test is performed in order to verify whether this is a 
            repeated frequency in the list.
        N)r   r   �name�value)r   r   r   r   r   �add_frequencyv   s    zNgram.add_frequencyc                 C   s   | j �|� dS )z�Add source information.
        Example: ["1:3,4,5", "7:3,2"].
        Meaning: ID 1 (words 3, 4 and 5) and ID 7 (words 3 and 2).
        N)r   r&   )r   r   r   r   r   �add_sources�   s    zNgram.add_sourcesc                 C   s   t �dd� | D ��S )a  
            Converts this ngram to an internal string representation where each
            word is separated from each other by a `WORD_SEPARATOR` and each
            part of the word is separated with a special `SEPARATOR`. This is 
            only used internally by the scripts and is of little use to the
            user because of reduced legibility. Deconversion is made by the 
            function `from_string`.
            
            @return A string with a special internal representation of the 
            ngram.
        c                 s   s   | ]}|� � V  qd S �N)�	to_stringr   r   r   r   �	<genexpr>�   s     z"Ngram.to_string.<locals>.<genexpr>)r   r#   �r   r   r   r   r0   �   s    zNgram.to_stringc                 C   s:   t �|t�}|D ]$}ti |d�}|�|� | �|� qdS )av   
            Instanciates the current ngram by converting to an object 
            an internal string representation where each word is separated from 
            each other by a `WORD_SEPARATOR` and each part of the word is 
            separated with a special `SEPARATOR`. This is only used internally 
            by the scripts and is of little use to the user because of reduced 
            legibility. Deconversion is made by the function `to_string`.
            
            @param the_string A string with a special internal representation of 
            the ngram, as generated by the function `to_string`
        )�ctxinfoN)r   �decent_str_splitr   r   �from_stringr*   )r   r3   Z
the_stringZwords_stringZword_stringZa_wordr   r   r   r5   �   s
    
zNgram.from_stringc                 C   s
   t | j�S r/   )�iterr   r2   r   r   r   �__iter__�   s    zNgram.__iter__c                 C   s
   t | j�S )a+  
            Returns the size of the ngram in number of words, i.e. the value of 
            n. An ngram with 2 words is called bigram and has n=2. An ngram with
            3 words is called trigram and has n=3.
            
            @return The number of words contained in the ngram.
        )�lenr   r2   r   r   r   �__len__�   s    zNgram.__len__c                 C   s
   | j | S )a  
            Returns a `Word` corresponding to the index `i` in the ngram. If the
            index i does not exist, will generate a `IndexError`.
            
            @param i The index i corresponding to the position of the searched
            word. If i=2, for instance, will return the 3rd word (indices start
            at zero) of the ngram.
            
            @return A `Word` at the i-th position of the ngram, or generates 
            IndexError if the position i is larger than the ngram size.
        �r   )r   �ir   r   r   �__getitem__�   s    zNgram.__getitem__c                 C   s   dd� | D �S )Nc                 S   s   g | ]
}|j �qS r   )r   r   r   r   r   r   �   s     z!Ngram._cmpkey.<locals>.<listcomp>r   r2   r   r   r   �_cmpkey�   s    zNgram._cmpkeyc                 C   s   t �| |�S r/   )r   �wordlist_ltr'   r   r   r   �__lt__�   s    zNgram.__lt__c                 C   s   | j |j kS r/   r:   r'   r   r   r   �__eq__�   s    zNgram.__eq__c                 C   s   t | �� �S r/   )�hashr0   r2   r   r   r   �__hash__�   s    zNgram.__hash__c                 C   s4   d}| D ]}||j  t }q|dt|�tt� � S )a�  
            Returns the sequence of Part Of Speech tags of this ngram 
            concatenated with an intervening `SEPARATOR`. For example, if the
            ngram is a sequence of one adjective (A) and two nouns (N), will 
            probably return something like "A#S#N#S#N", where "#S#" is the 
            separator.
            
            @return A string that corresponds to the sequence of POS tags of the
            ngram's words. The POS tags are joined with a special `SEPARATOR`
            defined in `__common.py`. Please pay attention that no POS tag 
            should be similar to the separator, to avoid ambiguities.
        r   r   )�posr   r8   )r   �resultr)   r   r   r   �get_pos_pattern�   s    zNgram.get_pos_patternc                 C   s   | D ]}|� |� qdS )z`For every word in this Ngram, delete all
        properties that are not in `prop_set`.
        N)�keep_only_props)r   �prop_setr)   r   r   r   rF   �   s    zNgram.keep_only_props)�	prop_namec                 C   s   | D ]}|� |� qdS )z-Delete property for every word in this Ngram.N)�del_prop)r   rH   r)   r   r   r   �foreach_del_prop  s    zNgram.foreach_del_propc                 C   s$   | j D ]}|j|kr|j  S qdS )aN  
            Returns the value of a `Frequency` in the frequencies list. The 
            frequency is identified by the frequency name provided as input to 
            this function. If two frequencies have the same name, only the first 
            value found will be returned.
            
            @param freq_name A string that identifies the `Frequency` of the 
            candidate for which you would like to know the value.
            
            @return Value of the searched frequency. If there is no frequency 
            with this name, then it will return 0.
        r   )r   r+   r,   )r   �	freq_namer   r   r   r   �get_freq_value  s    

zNgram.get_freq_valuec                 C   s6   | j D ]}|j|kr||_ dS q| �t||�� dS )a�  
            Updates the value of a `Frequency` in the frequencies list. The 
            frequency is identified by the frequency name. If two frequencies 
            have the same name, only the first will be updated. If no frequency
            has the given name, a new one is added to the ngram.
            
            @param freq_name A string that identifies the `Frequency` of the 
            candidate which you would like to update (or add, if it does not
            exist).
            
            @param freq_value Value of the frequency. If there is no frequency 
            with this name, then it will return 0.
        N)r   r+   r,   r-   r	   )r   rK   Z
freq_valuer   r   r   r   �update_freq_value   s
    

zNgram.update_freq_valueFc                 C   s�   d}d}t |�}| D ]x}|j}|r,|�d� ||krV|d }|�|d �rPd}q�d}n0|�|| �rn|d }n|�|d �r�d}nd}||_q||kr�|d }|S )a�  
            Matches the current `Ngram` with another `Ngram` given as parameter.
            The return, instead of boolean, is an integer that corresponds to 
            the number of times `an_ngram` occurs in the current ngram.
            
            @param an_ngram An `Ngram` that is going to be searched within the
            current `Ngram` (therefore, shorter or equal to it)
            
            @param ignore_pos If True, ngrams are counted regardless of POS tag,
            otherwise ngrams with different POS sequences are considered 
            different (default).
            
            @return The number of times that `an_ngram` was found in the current
            `Ngram`.            
        r   rC   r   �r8   rC   rI   �match)r   �an_ngram�
ignore_posr;   Zresult_count�nr   �bef_posr   r   r   �count6  s*    

zNgram.countc                 C   s�   d}t |�}| }| D ]f}|j}|r.|�d� ||kr>|  S |�|| �rV|d }n|�|d �rjd}nd}||_|d }q||kr�|S dS )a�  
            Matches the current `Ngram` with another `Ngram` given as parameter.
            The return value is an integer that corresponds to the position in
            the ngram where the first instance of `an_ngram` was found.
            
            @param an_ngram An `Ngram` that is going to be searched within the
            current `Ngram` (therefore, shorter or equal to it)
            
            @param ignore_pos If True, ngrams are counted regardless of POS tag,
            otherwise ngrams with different POS sequences are considered 
            different (default).
            
            @return The position in the current `Ngram` where the first instance 
            of `an_ngram` was found.            
        r   rC   r   �����rN   )r   rP   rQ   r;   rR   Z
result_posr   rS   r   r   r   �findd  s&    


z
Ngram.findc                 C   sL   t |�t | �krDtt | ��D ]"}| | j|| ||d�s dS qdS dS dS )ac  
            A simple matching algorithm that returns true if ALL the words of
            the current pattern match all the words of the given ngram. Since a 
            pattern does generally contain wildcards to express loose
            constraints, the matching is done at the word level considering only
            the parts that are defined, for example, POS tags for candidate
            extraction or lemmas for automatic gold standard evaluation.
            
            @param some_ngram A `Ngram` against which we would like to compare
            the current pattern. In general, the pattern contains wildcards
            while `some_ngram` has all the elements with a defined value.

            @return Will return True if ALL the words of `some_ngram` match ALL
            the words of the current pattern (i.e. they have the same number of
            words and all of them match in the same order). Will return False if
            the ngrams have different sizes or if ANY of the words of 
            `some_ngram` does not match the corresponding word of the current 
            pattern.
        )�ignore_case�lemma_or_surfaceFTN)r8   �rangerO   )r   Z
some_ngramrW   rX   r;   r   r   r   rO   �  s    zNgram.match)NNN)F)F)FF)r"   �
__module__�__qualname__�__doc__r   r$   r%   r*   r-   r.   r0   r5   r7   r9   r<   r=   r?   r@   rB   rE   rF   �strrJ   rL   rM   rT   rV   rO   r   r   r   r   r
   *   s0   

		

.
(r
   )r\   r   r   r   �featurer   r)   r   r   r   Z	frequencyr	   �objectr
   r   r   r   r   �<module>   s   	